<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use DateTime;

class GiftResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'giftName' => $this->name,
            // DateTime probably better performance?
            #'createdAt' => strtotime($this->created_at),
            'createdAt' => (int)(new DateTime($this->created_at))->format('U'),
        ];
    }
}
