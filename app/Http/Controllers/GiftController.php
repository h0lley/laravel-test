<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\GiftModel;
use App\Http\Resources\GiftResource;


class GiftController extends Controller
{
    public function create(Request $request)
    {
        $request->validate(['name' => 'required'], ['name.required' => 'Please specify a name.']);

        $gift = new GiftModel;
        $gift->name = $request->name;
        $gift->save();

        return response()->json(['message' => 'Success.']);
    }

    public function list()
    {
        return GiftResource::collection(GiftModel::all());
    }
}
